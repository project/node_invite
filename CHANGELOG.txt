
Node Invite 6.x-1.x-beta11 - Sept 1, 2009
----------------------------------
#556854 - Fix invalid HTML in default invite message
#558142 - Switched menu items to use dynamic arguments
#558634 - Using hook_content_extra_fields to allow for positioning of node_invite form elemens on the node forms.
#563818 - Fixing RSVP page token bug
Ran through the coder module recommendations across the entire module.
