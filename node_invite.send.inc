<?php
/**
 * @file
 * Provides the invite sending form.
 */

function node_invite_send($form_state, $nid) {
  global $user;

  if (!intval($nid)) {
    drupal_set_message("$nid is not a number. This form should only be called with a node-id as the 2nd argument in the URL.", 'error');
    return;
  }

  $form['your_name'] = array(
    '#title' => t('Your Name'),
    '#type' => 'textfield',
    '#default_value' => $user->name,
  );
  $form['your_email'] = array(
    '#title' => t('Your E-mail Address'),
    '#type' => 'textfield',
    '#default_value' => $user->mail,
  );
  $form['personal_message'] = array(
    '#title' => t('Personal Message'),
    '#type' => 'textarea',
    '#description' => t('Your personal message is going to be HTML formatted if you are using the HTMLMail module.  In addition, the personal message can take advantage of tokens.  See Token Help below.')
  );
  $form['emails'] = array(
    '#title'       => t('Email Addresses for Delivery'),
    '#type'        => 'textarea',
    '#description' => t('One recipient per line please.  Recipients can be entere as Name|Email or just an email address.')
  );

  $form['node_invite_tokens'] = array(
    '#type'  => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#description' => "<p>Not all tokens will be available.  The following lists all tokens known to Drupal.  The list of <i>available</i> tokens will depend on the type of node to which you are inviting people.</p>" . theme_token_help('all'),
    '#title' => ("Token Help"),
  );


  $form['nid'] = array(
    '#default_value' => $nid,
    '#type' => 'hidden',
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );

  return $form;
}

function node_invite_send_validate(&$form, $form_state) {
  // validate sender
  if (!valid_email_address($form_state['values']['your_email'])) {
    form_set_error('your_email', $form_state['values']['your_email'] . ' is not a valid email address');
  }
  // validate recipients
  $emails = preg_split("/[\n]/", $form_state['values']['emails']);
  foreach ($emails as $email) {
    $split = preg_split('/\s*\|\s*/', $email);
    if (count($split) == 2) {
      $email = $split[1];
    }
    if ($email != '' && !valid_email_address($email)) {
      form_set_error('emails', "$email is not a valid email address");
    }
  }
}

function node_invite_send_submit($form, &$form_state) {
  global $user;

  // 0> parse $from into something purty
  $from_string = $form_state['values']['your_email'];
  if ($form_state['values']['your_name']) {
    $from_string = check_plain( $form_state['values']['your_name'] ) . ' <' . $form_state['values']['your_email'] . '>';
  }

  // 1. Grab default message (or as of new version, also may need to see if there's an override on this node)
  $default_message = variable_get('node_invite_message_default', NODE_INVITE_MESSAGE_DEFAULT);
  $default_subject = variable_get('node_invite_subject_default', NODE_INVITE_SUBJECT_DEFAULT);

  // check for per-node overrides to subject and message
  $sth = db_query("SELECT subject, message FROM {node_invite_settings} WHERE nid = %d", $form_state['values']['nid']);
  if ($row = db_fetch_array($sth)) {
    if ($row['subject'] != '') {
      $default_subject = $row['subject'];
    }
    if ($row['message'] != '') {
      $default_message = $row['message'];
    }
  }

  // 2. Load the node
  $node = node_load($form_state['values']['nid']);

  // 3. do emails
  $emails = preg_split("/[\n]/", $form_state['values']['emails']);
  $send_count = 0;
  foreach ($emails as $email) {
    // on my mac, that preg_split makes:
    // array( 'a@b.c','',d@e.f','','g@h.i');
    // the empty check fixes that weirdness
    if ($email != '') {
      $split = preg_split('/\s*\|\s*/', $email);
      $recip_name = '';
      $recip_mail = $email;
      if (count($split) == 2) {
        $email = "$split[0] <$split[1]>";
        $recip_name = $split[0];
        $recip_mail = $split[1];
      }
      watchdog('node_invite', 'TO: ' . check_plain($email));

      // 4. store a record in {node_invites} and get insrt_id (which we use to build the message)
      db_query("INSERT INTO {node_invites} ( nid,email_invitee,uid_inviter,status,sent ) VALUES ( %d,'%s',%d,'%s',%d )", $form_state['values']['nid'], $email, $user->uid, 'NEW', time());
      $insert_id = db_last_insert_id('node_invites', 'iid');

      // 4.5 we need to send the invite stuff to token_replace (so it can drp in names, etc.
      // this also over-rides the old (and right) way of doing this where we pulled inviter-name/mail from global $user
      $node->invite_specific_info = array(
        'node-invite-iid' => $insert_id,
        'node-invite-recip-name' => $recip_name,
        'node-invite-recip-mail' => $recip_mail,
        'inviter-name' => $form_state['values']['your_name'],
        'inviter-mail' => $form_state['values']['your_email'],
      );
      //drupal_set_message("our stuff is: <PRE>" . print_r( $node->invite_specific_info, TRUE ) . "</pre>");

      // 5. build the email message for this user (link is different for every specific message... which is slow (re-parse every single message)
      $inviter_rsvp_url = url( 'node_invite/rsvp/' . $form_state['values']['nid'] . '/' . $insert_id, array('absolute' => TRUE) );
      $new_default_message = str_replace("[inviter-rsvp-url]", $inviter_rsvp_url, $default_message);
      $replaced_message = token_replace( $new_default_message, 'node', $node );
      $replaced_message .= "\n<br />" . token_replace($form_state['values']['personal_message'], 'node', $node);
      $subj = token_replace(
        $default_subject,
        'node',
        $node
      );

      $msg = drupal_mail(
        // module
        'node_invite',
        // key
        'invite',
        // to
        $email,
        // language
        language_default(),
        // params
        array(
          'subject' => $subj,
          'body'    => $replaced_message,
        ),
        // from
        $from_string,
        // send
        TRUE
      );
      $send_count++;
    }
  }

  // send them somewhere... back to the node!
  drupal_set_message($send_count . ' messages sent');
  $form_state['redirect'] = "node/" . $form_state['values']['nid'];
}

