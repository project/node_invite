<?php
/**
 * @file
 * Provides the invite rsvp response form.
 */

function node_invite_rsvp($form_state, $node, $iid) {
  // this form is probably being accessed by anonymous users.

  // make sure that iid is a number.
  if ( !intval($iid) ) {
    drupal_set_message("$iid is not a number. This form should only be called with a invite-id as the 3rd argument in the URL.", 'error');
    return;
  }

  $q = db_query("SELECT * FROM {node_invites} WHERE nid = %d AND iid = %d", $node->nid, $iid);
  $invite = db_fetch_array( $q );
  if ( !$invite ) {
    drupal_set_message(t("This URL is invalid (iid/nid mismatch)"), 'error');
    return;
  }

  $account = user_load($invite['uid_inviter']);
  $node->invite_specific_info = array(
    'node-invite-iid' => $invite['iid'],
    'node-invite-recip-name' => $invite['email_invitee'],
    'node-invite-recip-mail' => $invite['email_invitee'],
    'inviter-name' => $account->name,
    'inviter-mail' => $account->mail,
  );

  $page_title = token_replace(
    variable_get('node_invite_rsvp_page_title', NODE_INVITE_RSVP_PAGE_TITLE),
    'node',
    $node
  );
  drupal_set_title($page_title);

  $form['intro'] = array(
    '#value' => token_replace(
      variable_get('node_invite_rsvp_page_intro', NODE_INVITE_RSVP_PAGE_INTRO),
      'node',
      $node
    ),
  );

  /* form should contain:
        - Your email
        - Your comments
        - Hidden: iid
        - Hidden: nid (for redirect)
        - Status (drop-down of Yes, No, Maybe)
  */

  if (intval(variable_get('node_invite_rsvp_email', '0'))) {
    $form['email'] = array(
      '#type' => 'textfield',
      '#title' => t('Your Email'),
      '#description' => t('Please enter the email address to which the original invite was sent.  This allows us to make sure you are the actual recipient of the invite.'),
      '#required' => TRUE,
    );
  }
  if (intval(variable_get('node_invite_rsvp_show_comments', '0'))) {
    $form['comments'] = array(
      '#type' => 'textarea',
      '#title' => t('Comments'),
      '#description' => t('Use this field if you wish to leave a note for the person who invited you.'),
    );
  }

  $form['pretext'] = array(
    '#value' => token_replace(
      variable_get('node_invite_rsvp_page_submit_pretext', NODE_INVITE_RSVP_PAGE_SUBMIT_PRETEXT),
      'node',
      $node
    )
  );
  $form['yes'] = array(
    '#type' => 'submit',
    '#value' => t('Yes'),
  );
  $form['no'] = array(
    '#type' => 'submit',
    '#value' => t('No'),
  );
  $form['maybe'] = array(
    '#type' => 'submit',
    '#value' => t('Maybe'),
  );

  // hidden vars for tracking
  $form['nid'] = array(
    '#type' => 'value',
    '#value' => $node->nid
  );
  $form['iid'] = array(
    '#type' => 'value',
    '#value' => $iid,
  );

  return $form;
}

function node_invite_rsvp_validate($form, &$form_state) {
  if (intval(variable_get('node_invite_rsvp_email', '0'))) {
    if (!valid_email_address($form_state['values']['email'])) {
      // email is not valid.... regardless of whether or not it matches.  send them bck.
      form_set_error('email', $form_state['values']['email'] . ' is not valid. Please try again.');
    }
    else {
      $email = db_result(db_query("SELECT email_invitee FROM {node_invites} WHERE iid = %d AND nid = %d", $form_state['values']['iid'], $form_state['values']['nid']));
      if ($form_state['values']['email'] != $email) {
        form_set_error('email', $form_state['values']['email'] . ' is not the address to which this invite was sent.  Please use the <i>exact</i> email to which this invite was sent.');
      }
    }
  }
}

function node_invite_rsvp_submit($form, &$form_state) {
  db_query("UPDATE {node_invites} SET status = '%s', acted_upon = %d, notes_invitee = '%s' WHERE iid = %d", $form_state['values']['op'], time(), $form_state['values']['comments'], $form_state['values']['iid']);

  $node = node_load($form_state['values']['nid']);
  $admin_notify_setting = variable_get('node_invite_nots_admin', 'site_admin_all');
  if ($admin_notify_setting == 'site_admin_all') {
    /*
    $from = $to = $GLOBALS['conf']['site_mail'];
    $subject = token_replace(
      variable_get('node_invite_notify_site_admin_all_subject',NODE_INVITE_NOTIFY_SITE_ADMIN_ALL_SUBJECT),
      'node',
      $node
    );
    $the_message = token_replace(
      variable_get('node_invite_notify_site_admin_all',NODE_INVITE_NOTIFY_SITE_ADMIN_ALL),
      'node',
      $node
    );
    $msg = drupal_mail(
      // module
      'node_invite',
      // key
      'rsvp_site_admin_all',
      // to
      $to,
      // language
      language_default(),
      // params
      array(
        'subject' => $subject,
        'body'    => $the_message,
      ),
      // from
      $from,
      // send
      TRUE
    );
    */
  }
  elseif ($admin_notify_setting == 'site_admin_yes' && $form_state['values']['op'] == t('Yes')) {
    /*
    $the_message = token_replace(
      variable_get('node_invite_notify_site_admin_yes',NODE_INVITE_NOTIFY_SITE_ADMIN_YES),
      'node',
      $node
    );
    $subject = token_replace(
      variable_get('node_invite_notify_site_admin_yes_subject',NODE_INVITE_NOTIFY_SITE_ADMIN_YES_SUBJECT),
      'node',
      $node
    );
    $from = $to = $GLOBALS['conf']['site_mail'];
    $msg = drupal_mail(
      // module
      'node_invite',
      // key
      'rsvp_site_admin_yes',
      // to
      $to,
      // language
      language_default(),
      // params
      array(
        'subject' => $subject,
        'body'    => $the_message,
      ),
      // from
      $from,
      // send
      TRUE
    );
    */
  }

  // notify the inviter
  $notify_settings = variable_get('node_invite_nots_rsvps', array('inviter', 'invitee'));
  if (in_array('inviter', $notify_settings)) {
    $the_message = token_replace(
      variable_get('node_invite_notify_inviter_notify', NODE_INVITE_NOTIFY_INVITER_NOTIFY),
      'node',
      $node
    );
    $from = $GLOBALS['conf']['site_mail'];
    $to = db_result(db_query("SELECT u.mail FROM {users} u JOIN {node_invites} ni ON u.uid = ni.uid_inviter WHERE iid = %d", $form_state['values']['iid']));
    $subject = token_replace(
      variable_get('node_invite_notify_inviter_notify_subject', NODE_INVITE_NOTIFY_INVITER_NOTIFY_SUBJECT),
      'node',
      $node
    );
    $msg = drupal_mail(
      // module
      'node_invite',
      // key
      'rsvp_inviter',
      // to
      $to,
      // language
      language_default(),
      // params
      array(
        'subject' => $subject,
        'body'    => $the_message,
      ),
      // from
      $from,
      // send
      TRUE
    );
  }

  // redirect to the node.
  drupal_goto('node/' . $form_state['values']['nid']);
}

