<?php
/**
 * @file
 * Node invite admin settings
 */

function node_invite_admin_settings_form() {
  $form['node_invite_node_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Which node types can have invites?'),
    '#description' => t('Use this to enable/disable node types that can have invites associated with them'),
    '#options' => node_get_types('names'),
    '#default_value' => variable_get('node_invite_node_types', array()),
    '#weight' => -10,
  );
  $form['node_invite_default'] = array(
    '#weight' => -9,
    '#title' => t('Default behavior on enabled node-types'),
    '#type' => 'select',
    '#options' => array(
      '1' => 'Enable invites',
      '0' => 'Disable invites',
    ),
    '#default_value' => variable_get('node_invite_default', NODE_INVITE_DEFAULT_FOR_ENABLED_NODES),
    '#description' => t('
      <strong>Enable Invites</strong> = All nodes of an enabled type will have a $node->link[\'\'] unless you un-check the "Enable invites for this node" box in the node/edit form<br />
      <strong>Disable Invites</strong> = To get an invite for a specific node, you will need to check the "Enable invites for this node" box in the node/edit form
      '),
  );
  $form['node_invite_rsvp_page'] = array(
    '#type' => 'fieldset',
    '#title' => t('RSVP Page'),
    '#description' => t('Users who receive an invite, will be directed to a page asking them to accept the invite.  The parts of that page are configurable here.'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['node_invite_rsvp_page']['node_invite_rsvp_show_comments'] = array(
    '#type' => 'select',
    '#options' => array(
      '1' => 'Yes',
      '0' => 'No',
    ),
    '#default_value' => variable_get('node_invite_rsvp_show_comments', '0'),
    '#title' => t('Allow invitees to leave comments?'),
    '#description' => t('This causes a text-area to show up on the RSVP form.'),
  );
  $form['node_invite_rsvp_page']['node_invite_rsvp_email'] = array(
    '#type' => 'select',
    '#title' => t('Force user to re-enter their email?'),
    '#description' => t('Optional security measure'),
    '#options' => array(
      '1' => 'Yes',
      '0' => 'No',
    ),
    '#default_value' => variable_get('node_invite_rsvp_email', '0'),
  );
  $form['node_invite_rsvp_page']['node_invite_rsvp_page_submit_pretext'] = array(
    '#type' => 'textarea',
    '#title' => t('Text to display before submit buttons'),
    '#default_value' => variable_get('node_invite_rsvp_page_submit_pretext', NODE_INVITE_RSVP_PAGE_SUBMIT_PRETEXT),
    '#description' => t('Tokens and html accepted.'),
  );
  $form['node_invite_rsvp_page']['node_invite_rsvp_page_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Page Title'),
    '#default_value' => variable_get('node_invite_rsvp_page_title', NODE_INVITE_RSVP_PAGE_TITLE),
    '#description' => t('The subject is token-enabled. Please use the list below for replacement patterns.'),
  );
  $form['node_invite_rsvp_page']['node_invite_rsvp_page_intro'] = array(
    '#type' => 'textarea',
    '#title' => t('Page Intro Text'),
    '#default_value' => variable_get('node_invite_rsvp_page_intro', NODE_INVITE_RSVP_PAGE_INTRO),
    '#description' => t('This block of text will lead off the form. The subject is token-enabled. Please use the list below for replacement patterns.'),
  );

  $form['node_invite_message'] = array(
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#title' => t('Invite Message'),
  );
  $form['node_invite_message']['node_invite_message_default'] = array(
    '#type' => 'textarea',
    '#title' => t('E-Mail Sent to Invitees'),
    '#description' => t('The email we send is token-enabled. Please use the list below for replacement patterns.'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#default_value' => variable_get('node_invite_message_default', NODE_INVITE_MESSAGE_DEFAULT),
    '#weight' => 0,
  );
  $form['node_invite_message']['node_invite_subject_default'] = array(
    '#type' => 'textfield',
    '#title' => t('Subject for emails'),
    '#description' => t('The subject is token-enabled. Please use the list below for replacement patterns.'),
    '#default_value' => variable_get('node_invite_subject_default', NODE_INVITE_SUBJECT_DEFAULT),
    '#weight' => -1,
  );
  $form['node_invite_tokens'] = array(
    '#type'  => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#description' => "<p>Not all tokens will be available. The following lists all tokens known to Drupal.  The list of <i>available</i> tokens will depend on the type of node to which you are inviting people.</p>" . theme_token_help('all'),
    '#title' => ("Token Help"),
  );

  return system_settings_form($form);
}

function node_invite_notifications_form() {
  $form['node_invite_intro'] = array(
    '#value' => '<p>This form allows you to manage notifications set out by the node_invite module</p>'
  );
  $form['node_invite_nots_rsvps'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Who should we notify when someone RSVPs?'),
    '#options' => array(
      'invitee' => '<strong>Invitee:</strong> Upon selection of any option, the invitee should receive an email confirming their selection (regardless of Yes/No/Maybe)',
      'inviter' => '<strong>Inviter:</strong> The person who sent the original invite should receive an email notifying them that someone answer their invite (regardless of Yes/No/Maybe)',
    ),
    '#default_value' => variable_get('node_invite_nots_rsvps', array('inviter', 'invitee')),
  );
  $form['node_invite_nots_admin'] = array(
    '#type' => 'radios',
    '#title' => t('When should we notify the site admin of an invite response?'),
    '#options' => array(
      'site_admin_never' => t('Never notify the site admin of an invite response.'),
      'site_admin_yes' => t('Site Admin User should receive an email whenever someone ACCEPTS an invite'),
      'site_admin_all' => t('Site Admin User should receive an email whenever someone responds (with any answer) to an invite'),
    ),
    '#default_value' => variable_get('node_invite_nots_admin', 'site_admin_all'),
  );
  $form['node_invite_nots_sep'] = array(
    '#value' => '<h1>Messages</h1>',
  );
  $form['node_invite_fs_site_admin_yes'] = array(
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#title' => t('Site Admin User should receive an email whenever someone ACCEPTS an invite'),
  );
  $form['node_invite_fs_site_admin_yes']['node_invite_notify_site_admin_yes_subject'] = array(
    '#type' => 'textfield',
    '#title' => t('Subject'),
    '#description' => t('Token enabled'),
    '#default_value' => variable_get('node_invite_notify_site_admin_yes_subject', NODE_INVITE_NOTIFY_SITE_ADMIN_YES_SUBJECT),
  );
  $form['node_invite_fs_site_admin_yes']['node_invite_notify_site_admin_yes'] = array(
    '#type' => 'textarea',
    '#description' => t('Token enabled'),
    '#title' => t('Notification Message for this situation'),
    '#default_value' => variable_get('node_invite_notify_site_admin_yes', NODE_INVITE_NOTIFY_SITE_ADMIN_YES),
  );
  $form['node_invite_fs_site_admin_all'] = array(
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#title' => t('Site Admin User should receive an email whenever someone responds (with any answer) to an invite'),
  );
  $form['node_invite_fs_site_admin_all']['node_invite_notify_site_admin_all_subject'] = array(
    '#type' => 'textfield',
    '#title' => t('Subject'),
    '#description' => t('Token enabled'),
    '#default_value' => variable_get('node_invite_notify_site_admin_all_subject', NODE_INVITE_NOTIFY_SITE_ADMIN_ALL_SUBJECT),
  );
  $form['node_invite_fs_site_admin_all']['node_invite_notify_site_admin_all'] = array(
    '#type' => 'textarea',
    '#description' => t('Token enabled'),
    '#title' => t('Notification Message for this situation'),
    '#default_value' => variable_get('node_invite_notify_site_admin_all', NODE_INVITE_NOTIFY_SITE_ADMIN_ALL),
  );
  $form['node_invite_fs_invitee_confirm'] = array(
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#title' => t('Upon selection of any option, the invitee should receive an email confirming their selection (regardless of Yes/No/Maybe)'),
  );
  $form['node_invite_fs_invitee_confirm']['node_invite_notify_invitee_confirm_subject'] = array(
    '#type' => 'textfield',
    '#title' => t('Subject'),
    '#description' => t('Token enabled'),
    '#default_value' => variable_get('node_invite_notify_invitee_confirm_subject', NODE_INVITE_NOTIFY_INVITEE_CONFIRM_SUBJECT),
  );
  $form['node_invite_fs_invitee_confirm']['node_invite_notify_invitee_confirm'] = array(
    '#type' => 'textarea',
    '#description' => t('Token enabled'),
    '#title' => t('Notification Message for this situation'),
    '#default_value' => variable_get('node_invite_notify_invitee_confirm', NODE_INVITE_NOTIFY_INVITEE_CONFIRM),
  );
  $form['node_invite_fs_inviter_notify'] = array(
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#title' => t('The person who sent the original invite should receive an email notifying them that someone answer their invite (regardless of Yes/No/Maybe)'),
  );
  $form['node_invite_fs_inviter_notify']['node_invite_notify_inviter_notify_subject'] = array(
    '#type' => 'textfield',
    '#title' => t('Subject'),
    '#description' => t('Token enabled'),
    '#default_value' => variable_get('node_invite_notify_inviter_notify_subject', NODE_INVITE_NOTIFY_INVITER_NOTIFY_SUBJECT),
  );
  $form['node_invite_fs_inviter_notify']['node_invite_notify_inviter_notify'] = array(
    '#type' => 'textarea',
    '#description' => t('Token enabled'),
    '#title' => t('Notification Message for this situation'),
    '#default_value' => variable_get('node_invite_notify_inviter_notify', NODE_INVITE_NOTIFY_INVITER_NOTIFY),
  );
  $form['node_invite_tokens'] = array(
    '#type'  => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#description' => theme_token_help('node'),
    '#title' => t('Available Tokens'),
  );

  return system_settings_form($form);
}
